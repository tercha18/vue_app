export interface Pager {
    itemsPerPageArray: number[];
    page: number;
    itemsPerPage: number;
    itemsCount: number;
}