import { Pager } from '@/helpers/pager';
import axios from 'axios';

/**
 * PeopleService
 */
export default class PeopleService {

    public static async getList(name: string, pager: Pager): Promise<any> {
        const url = 'https://swapi.co/api/people/?search=' + (name.length > 0 ? name : '')+ '&page=' + pager.page;
        return (
            await axios({
                method: 'get',
                headers: { 'Content-Type': 'application/json' },
                url: url
            }).then(function (response) {
                return response.data;
            }));
    }
}

