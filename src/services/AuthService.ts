import Vue from 'vue';
import store from '@/store/index';
import router from '@/router';

/**
 * AuthService
 */
export default class AuthService {
    public static Login(model: LoginModel): boolean {
        if (store.state.login !== model.username) {
            return false;
        }

        if (store.state.password !== model.password) {
            return false;
        }
        store.state.isLogged = true;
        this.SetLocalStorage();
        return true;
    }

    public static Logout(): void {
        store.state.isLogged = false;
        this.ClearLocalStorage();
        router.push('Login');
    }

    private static SetLocalStorage(): void {
        localStorage.setItem('Login', 'Tester');
        localStorage.setItem('Token', 'akdjqe123sdgdfh');
    }

    private static ClearLocalStorage(): void {
        localStorage.clear();
    }
}

export interface LoginModel {
    username: string;
    password: string;
}
