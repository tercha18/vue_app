import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    login: 'test',
    password: 'test_pass',
    isLogged: false
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  },
});
